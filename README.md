## Objectives

1. Students should review Ruby Strings and the CLI
2. Students should learn about writing methods in Ruby
3. Students will apply all that they learned to write a more complex ruby program


## Resources

* [Intro to Ruby Methods](https://github.com/learn-co-curriculum/ruby-methods-readme)
* [Video - Methods](https://www.youtube.com/embed/njJB-fuE-qE?rel=0&modestbranding=1)
* [Defining Ruby Methods](http://ruby-for-beginners.rubymonstas.org/writing_methods/definition.html)
* [More on Ruby Methods](http://ruby-for-beginners.rubymonstas.org/writing_methods/definition.html)
* [Method Arguments](https://github.com/learn-co-curriculum/ruby-arguments-readme)
* [Video - Method Arguments](https://www.youtube.com/embed/FJztnY9E854?rel=0&modestbranding=1)
* [Returning Values](https://github.com/learn-co-curriculum/puts-print-and-return-readme)
